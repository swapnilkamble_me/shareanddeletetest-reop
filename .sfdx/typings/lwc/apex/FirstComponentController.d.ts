declare module "@salesforce/apex/FirstComponentController.fetchAccountsNew" {
  export default function fetchAccountsNew(): Promise<any>;
}
declare module "@salesforce/apex/FirstComponentController.fetchAccountsWithContacts" {
  export default function fetchAccountsWithContacts(): Promise<any>;
}
