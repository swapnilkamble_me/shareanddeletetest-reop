declare module "@salesforce/apex/DeepCloneOpprotunityController.init" {
  export default function init(param: {oppId: any}): Promise<any>;
}
