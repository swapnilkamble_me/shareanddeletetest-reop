declare module "@salesforce/apex/PaginationController.findAll" {
  export default function findAll(param: {searchKey: any, pageNumber: any, sortByCol: any, sortOrder: any}): Promise<any>;
}
