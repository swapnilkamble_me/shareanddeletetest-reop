declare module "@salesforce/apex/SACAppController.init" {
  export default function init(param: {oppId: any}): Promise<any>;
}
