({
	doInit : function(component, event, helper) {
		var action = component.get('c.init');
        action.setParams({
            oppId : '0069000000BMLcy'
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                console.log(response.getReturnValue());
                component.set('v.opportunity', response.getReturnValue());
            } else {
                console.log(response.getError());
            }
        });
        
        $A.enqueueAction(action);
	}
})