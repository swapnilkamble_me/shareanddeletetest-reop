({
    setup : function(component, event, helper) {
        var j$ = jQuery.noConflict();
        j$( "#sortable1, #sortable2" ).sortable({
            connectWith: ".connectedSortable"
        }).disableSelection();
    }
})