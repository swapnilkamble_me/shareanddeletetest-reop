({
	getAccounts : function(component, page, sortByCol, sortOrder) {
        page = page || 1;
        debugger;
        var action = component.get("c.findAll");
		action.setParams({
      		"searchKey": component.get("v.searchKey"),
            "pageNumber": page,
            "sortByCol": sortByCol,
            "sortOrder":sortOrder
    	});
    	action.setCallback(this, function(a) {
            var result = a.getReturnValue();
            component.set("v.accounts", result.accounts);
            component.set("v.page", result.page);
            component.set("v.total", result.total);
            component.set("v.pages", Math.ceil(result.total/5));
    	});
    	$A.enqueueAction(action);
	}
})