({
	doInit: function(component, event, helper) {
        helper.getAccounts(component);
    },
    searchKeyChange: function(component, event, helper) {
        component.set("v.searchKey", event.getParam("searchKey"));
        helper.getAccounts(component);
	},	
	pageChange: function(component, event, helper) {
		var page = component.get("v.page") || 1;
        var direction = event.getParam("direction");
        var sortByCol = component.get("v.attrSortByCol");
        page = direction === "previous" ? (page - 1) : (page + 1);
        helper.getAccounts(component, page, sortByCol);
	},
    sortOrderChanged: function(component,event, helper){
        debugger;
    	var sortByCol = event.currentTarget.getAttribute("data-data");
        if(component.get("v.attrSortByCol") === sortByCol){
            var temp = component.get("v.attrSortOrder") =='ASC'? 'DESC' : 'ASC';
            component.set("v.attrSortOrder", temp);
        }
        component.set("v.attrSortByCol", sortByCol);
        helper.getAccounts(component, 1, sortByCol, component.get("v.attrSortOrder"));
	}
})