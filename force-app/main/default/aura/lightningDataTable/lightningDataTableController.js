({
    fetchAccounts : function(component, event, helper) {
        component.set('v.mycolumns', [
            {label: 'Account Name', fieldName: 'Name', type: 'text', sortable: true},
            {label: 'Industry', fieldName: 'Industry', type: 'text'},
            {label: 'Active', fieldName: 'Active__c', type: 'boolean'},
            {label: 'OwnerName', fieldName: 'OwnerLink', type: 'url', typeAttributes: {label: { fieldName: 'OwnerName' }, target: '_blank'}}
        ]);
        var action = component.get("c.fetchAccountsNew");
        action.setParams({
        });
        action.setCallback(this, function(response){
            debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
                
                
                var rows = response.getReturnValue();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.Owner.Name) row.OwnerName = row.Owner.Name;
                    if (row.OwnerId) row.OwnerLink = '/'+row.Owner.Id;
                }

                component.set("v.acctList", rows);
                helper.sortData(component, component.get("v.sortedBy"), component.get("v.sortedDirection"));
            }
        });
        $A.enqueueAction(action);
    },
    updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    }
})