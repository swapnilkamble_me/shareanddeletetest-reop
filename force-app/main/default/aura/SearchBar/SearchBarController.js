({
	searchKeyChange: function(component, event) {
        var myEvent = $A.get("e.c:PageSearchKeyChange");
        var searchKey = component.get("v.searchKey");
        myEvent.setParams({"searchKey": searchKey});
        myEvent.fire();
    },
    clearText: function(component) {
        component.set("v.searchKey", "");
        var myEvent = $A.get("e.c:PageSearchKeyChange");
        myEvent.setParams({"searchKey": "" });
        myEvent.fire();
    }
})