({
	fetchAccounts12 : function(component, event) {
        var action=component.get('c.fetchAccounts');
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
               // var data = JSON.parse(response.getReturnValue());
                component.set('v.data', response.getReturnValue());
            }
        });
        
        $A.enqueueAction(action);
	}
})