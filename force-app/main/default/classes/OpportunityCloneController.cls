public with sharing class OpportunityCloneController {
    public Opportunity oppty {get;set;}
    public List<Opportunity> ParentOpportunityList { get; set; }
    public List<String> FieldList { get; set; }
    public Boolean isError{get; set;}
    
    public OpportunityCloneController(ApexPages.StandardController controller) {
        this.oppty = (Opportunity)controller.getRecord();
        isError = false;
        FieldList = new List<String>{ 'Name',  'StageName',  'Type',  'Amount',  'CloseDate'};
        ParentOpportunityList = [SELECT Name, StageName, Type, Amount, CloseDate FROM Opportunity WHERE Id = :controller.getId()];
    }
    
    public PageReference cancel() {
        if(!ParentOpportunityList.isEmpty()){
            return new PageReference('/' + ParentOpportunityList[0].Id);
        }
        return NULL;
    }
    
    public void cloneOpportunity(){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Encountered some errors when trying to clone.'));
        //ApexPages.addMessages('test');

        isError = true;
    }
}