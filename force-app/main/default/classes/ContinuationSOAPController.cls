global class ContinuationSOAPController {
    public static AsyncContinuationClass.GetContinuationRespResponse_elementFuture stockQuoteFuture;
    public static Continuation cont;
    
    public static String result {get;set;}

    // Action method
    @RemoteAction
   public static Object startRequest() {    
       Integer TIMEOUT_INT_SECS = 60;  
       cont = new Continuation(TIMEOUT_INT_SECS);       
       cont.continuationMethod = 'processResponse';
       system.debug('*****inside startRequest');
       AsyncContinuationClass.AsyncContinuationTestWebservice stockQuoteService = new AsyncContinuationClass.AsyncContinuationTestWebservice();
       stockQuoteFuture = stockQuoteService.beginGetContinuationResp(cont,2); 
       cont.state =    stockQuoteFuture ;
       system.debug('*****inside cont'+cont);
       system.debug('*****inside stockQuoteFuture :'+stockQuoteFuture);
       return cont;   
    }    
    
    // Callback method
    @RemoteAction
    public static Object processResponse(Object p) {
    try{   
        system.debug('*****processResponse called stockQuoteFuture::'+stockQuoteFuture);
        stockQuoteFuture = (AsyncContinuationClass.GetContinuationRespResponse_elementFuture)p;
        result = stockQuoteFuture.getValue();
       
        //result =Continuation.getResponse((String)state);
       
       // Return null to re-render the original Visualforce page
       
       system.debug('*****ContinuationSOAPController ::'+result);
       //insert new Account(Name='AsyncAccount');
       //pagereference redirect = new PageReference('/apex/SubmitPopup');
       return result; 
       }
       catch(Exception e){
           result = 'from catch ========>';
           result += e.getMessage();
           return result;
       }
    }
    
    public boolean SyncContinuationCall(){
    
        ContinuationClass.ContinuationTestWebservice objContinuationClass  = new ContinuationClass.ContinuationTestWebservice();
        system.debug('*******Sync'+objContinuationClass.GetContinuationResp(2));
        return true;
    }
}