global class BatchSearch implements Database.Batchable<sObject>{

   global final String Query;

   global BatchSearch(){

      Query='SELECT Id, Name, NumberofLocations__c FROM Account WHERE Id =\'0019000001xHCYN\'';
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }

   global void execute(Database.BatchableContext BC, List<Account> acc){
     system.debug('scope ::'+acc);
       for(Account s : acc){
           s.NumberofLocations__c++ ;
       }
       update acc;
    }

   global void finish(Database.BatchableContext BC){
   }
}