public class PaginationController {
    @AuraEnabled
    public static PagedResult findAll(String searchKey, Decimal pageNumber, String sortByCol, String sortOrder) {
        Integer pageSize = 5;
        String key = String.isNotBlank(searchKey) ? '\'%' + searchKey + '%\'': '%'+searchKey+'%';
        Integer offset = ((Integer)pageNumber - 1) * pageSize;
        
        sortByCol = String.isNotBlank(sortByCol)?sortByCol:'Name';
        sortOrder = String.isNotBlank(sortOrder)?sortOrder:'ASC';
		
        
        PagedResult r =  new PagedResult();
        r.pageSize = pageSize;
        r.page = (Integer) pageNumber;
        r.total = [SELECT count() FROM Account 
                   WHERE (Name LIKE :key)];
        
        
        
        string querystr = '';
        querystr = 'SELECT Id, Name, Website, Phone, BillingState FROM Account ';
        if(String.isNotBlank(searchKey)){
           querystr += 'WHERE Name LIKE ' + key; 
        }
            
        querystr += ' ORDER BY ';
        querystr += sortByCol;
        querystr += ' '+sortOrder;
        querystr += ' LIMIT 5 ';
        querystr += 'OFFSET ';
        querystr += offset;
        
        /*r.accounts = [SELECT Id, Name, Website, Phone FROM Account 
                    WHERE Name LIKE :key
                    ORDER BY Name LIMIT 2 OFFSET :offset];*/
        system.debug('***querystr::'+querystr);
        
        r.accounts = Database.query(querystr);
        return r;
    }
}