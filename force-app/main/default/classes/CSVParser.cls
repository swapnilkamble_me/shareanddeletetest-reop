public class CSVParser {
    //https://developer.salesforce.com/forums/?id=906F000000090rbIAA
    //https://developer.salesforce.com/forums/?id=906F00000009024IAA
    public static void queryAttachment(){
        List<Attachment> attachList = [Select Id, Body FROM Attachment WHERE Id='00P0I000015ck2S'];
        //Blob b = EncodingUtil.base64Decode(attachList[0].Body);
        
        List<List<String>> allData = parseCSV(attachList[0].Body.toString(),true);
        List<Account>allNewAccounts = new List<Account>();
        for(List<String> lstRecord :allData){            
            allNewAccounts.add(dataClensing(lstRecord));
        }
        insert allNewAccounts;
    }
    
    private static Account dataClensing(list<string>lstRecord){
        List<string>fieldApiName = new List<string>{'Name','Phone','Active__c'};
            
            
        Account accountToInsert = new Account();
        /*for(integer i=0; i<lstRecord.size(); i++){
            
            Schema.DisplayType fldType = Schema.SObjectType.Account.fields.getMap().get(fieldApiName[i]).getDescribe().getType();
            string strFieldType = fldType.name().ToLowerCase();
            // Object instance = fldType.newInstance();
            if(lstRecord[i].startsWith('"') && lstRecord[i].endsWith('"')){
                //accountToInsert.put(fieldApiName[i],fldType.valueOf(lstRecord[i].removeStart('"').removeEnd('"'))); 
            }else{
                // accountToInsert.put(fieldApiName[i],fldType.valueOf(lstRecord[i]));
            }
        }*/
        
        system.debug('***********accountToInsert+'+accountToInsert);
        return accountToInsert;
    }
    
    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        List<List<String>> allFields = new List<List<String>>();
        
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try {
            lines = contents.split('\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        Integer num = 0;
        for(String line : lines) {
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) break;
            
            List<String> fields = line.split(',');	
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;		
    }
    
}