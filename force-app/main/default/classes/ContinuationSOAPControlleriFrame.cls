global class ContinuationSOAPControlleriFrame {
    global  AsyncContinuationClass.GetContinuationRespResponse_elementFuture stockQuoteFuture;
    
    public String result {get;set;}

    // Action method
   public Continuation startRequest() {    
       Integer TIMEOUT_INT_SECS = 60;  
       Continuation cont = new Continuation(TIMEOUT_INT_SECS);
       cont.continuationMethod = 'processResponse';
       system.debug('*****inside startRequest');
       AsyncContinuationClass.AsyncContinuationTestWebservice stockQuoteService = new AsyncContinuationClass.AsyncContinuationTestWebservice();
           stockQuoteFuture = stockQuoteService.beginGetContinuationResp(cont,2);     
       system.debug('*****inside cont'+cont);
       system.debug('*****inside stockQuoteFuture :'+stockQuoteFuture);
       return cont;   
    }    
    
    // Callback method
    
    public String processResponse() {
    try{   
        system.debug('*****processResponse called stockQuoteFuture::'+stockQuoteFuture);
        
       result = stockQuoteFuture.getValue();
       
       // Return null to re-render the original Visualforce page
       
       system.debug('*****ContinuationSOAPController ::'+result);
       //insert new Account(Name='AsyncAccount');
       //pagereference redirect = new PageReference('/apex/SubmitPopup');
       return null; 
       }
       catch(Exception e){
           result = 'from catch ========>';
           result += e.getMessage();
           return null;
       }
    }
    
    public boolean SyncContinuationCall(){
    
        ContinuationClass.ContinuationTestWebservice objContinuationClass  = new ContinuationClass.ContinuationTestWebservice();
        system.debug('*******Sync'+objContinuationClass.GetContinuationResp(2));
        
        return true;
    }
}