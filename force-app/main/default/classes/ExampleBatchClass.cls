global class ExampleBatchClass implements Database.Batchable<sObject>{
    
    global ExampleBatchClass(){
        // Batch Constructor
        
    }
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id From Account LIMIT 1');
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<sObject>scope){
        // Logic to be Executed batch wise      
        
    }
    
    global void finish(Database.BatchableContext BC){
        //system.debug('MINUTE='+system.now().minute()+'   Hour='+system.now().hour()+'   Day='+system.now().day()+'    Month'+system.now().month());
        
        
        
        DateTime newtime = system.now().addMinutes(1);
        String strTime = '0' +' '+string.valueof(newtime.minute()) +' '+ string.valueof(newtime.hour()) +' '+ string.valueof(newtime.day())+' '+string.valueof(newtime.month())+' ?'+' '+string.valueof(newtime.year());

        system.schedule('Job - '+strTime, strTime, new scheduledBatchable());
    }
}