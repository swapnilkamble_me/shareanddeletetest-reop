/* 
{
    "p": {
        "PerInfo": {
            "firstname": "amit",
            "lastname": "patel"
        },
        "Phone": {
            "type": "home",
            "no": "345345"
        }
    }
}
*/

@RestResource(urlMapping='/RestTestAPI/*')
global with sharing class RestTestAPI {
    
    @HttpPost
    global static Party createProspect(Party p) {
        RestRequest request = RestContext.request;
        //system.debug('RestRequest.requestBody--'+request.requestBody.toString());
        system.debug('RestRequest'+request);
        
        p.perInfo.firstname += 'updated';
        p.perInfo.lastname += 'updated';
        return p;      
    }
    
    global with sharing  class Party {
        private PerInfo perInfo; 
        private Phone phone ; 
    }
    global with sharing  class PerInfo {
        private String firstname;
        private String lastname;
    }
    global with sharing  class Phone {
        private String type;
        private String no;
    }
}