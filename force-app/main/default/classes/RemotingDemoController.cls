global class RemotingDemoController{
    
    @RemoteAction
    global static List<Account> getAccount() {
        try{
            return  [SELECT Id, Name FROM Account LIMIT 10];
       }catch(Exception e){
           system.debug('Exception in getAccount'+e.getMessage());
       }
        return null;
    }
}