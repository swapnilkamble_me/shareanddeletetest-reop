global class TestScheduledApexFromTestMethod implements Schedulable {

// This test runs a scheduled job at midnight Sept. 3rd. 2022

   //public static String CRON_EXP = '0 0 0 3 9 ? 2022';
   global string getJobName(){
        return 'testSchedule';
   }
    global Integer getCurrentJobCount(){
        return (Integer)[Select count() From AsyncApexJob Where JobType = 'Scheduled Apex' and ( Status = 'Processing' or Status = 'Preparing' )];
    }
   global void execute(SchedulableContext ctx) {
      if(getCurrentJobCount() != Null && getCurrentJobCount() > 4 ){
            Datetime sysTime = System.now().addMinutes(5);
            String chronExpression = '' + sysTime.second() + ' ' + sysTime.minute() + ' * * * ?';
            System.schedule( this.getJobName() + sysTime, chronExpression, (System.Schedulable)this );
            
        }else{
        	Database.executeBatch( new BatchSearch(), 1 );
        }
   }   
}