public class JavaScriptDemoController
{
	@RemoteAction
    public static Database.UpsertResult UpsertContact(Student__c student)
    {
        Database.UpsertResult upsertResult = Database.upsert(student);
        
        return upsertResult;
    }

}