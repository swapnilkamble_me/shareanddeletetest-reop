public virtual class ChildClass extends ParentClass{
    public ChildClass() {
        super();
        system.debug('Step 2');
    }
    public virtual override void method1() {
        system.debug('Step 4');
    } 
    public static void method2() {
        system.debug('Step 6');
    }
}