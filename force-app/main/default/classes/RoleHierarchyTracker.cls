public class RoleHierarchyTracker {
    // public with sharing class RoleHierarchy
    static Map<Id, List<UserRole>> hierarchy{
        get{
            if (hierarchy == null){
                hierarchy = new Map<Id, List<UserRole>>();
                for (UserRole role : [SELECT ParentRoleId FROM UserRole]){
                    if (!hierarchy.containsKey(role.ParentRoleId))
                        hierarchy.put(role.ParentRoleId, new List<UserRole>());
                    hierarchy.get(role.ParentRoleId).add(role);
                }
            }
            return hierarchy;
        }
        private set;
    }
    
    public static List<UserRole> getChildren(Id userRoleId){
        return hierarchy.containsKey(userRoleId) ? hierarchy.get(userRoleId) : new List<UserRole>();
    }
    
    public static Set<Id> getSubHierarchyInclusive(Id userRoleId){
        Set<Id> roleIds = new Set<Id> { userRoleId };
            
        for (UserRole childRole : getChildren(userRoleId)){
        	roleIds.addAll(getSubHierarchyInclusive(childRole.Id));    
        }
        return roleIds;
    }               
                 
}