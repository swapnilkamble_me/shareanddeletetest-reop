public class EditCustomSettingController {

    public void editCreds(){
        //Creds_for_Integration__c mcs = Creds_for_Integration__c.getInstance();
        Creds_for_Integration__c mcs = [SELECT id, UserName__c FROM Creds_for_Integration__c][0];
        system.debug('mcs'+mcs.UserName__c);
		mcs.UserName__c = 'Big Bang2';
        update mcs;
    }
    
    public void updateCreds(string text){
        //Creds_for_Integration__c mcs = Creds_for_Integration__c.getInstance();
        Creds_for_Integration__c mcs = [SELECT id, UserName__c FROM Creds_for_Integration__c][0];
        system.debug('mcs'+mcs.UserName__c);
		mcs.UserName__c = text;
        update mcs;
    }
    
    //EditCustomSettingController.encryptionByAes();
    public void encryptionByAes(){
        String PLAIN_TEXT = 'Some Value';
        Blob cryptoKey = Crypto.generateAesKey(256);
        //system.debug('cryptoKey : '+EncodingUtil.base64Decode(cryptoKey).toString());
        //Blob hash = Crypto.generateDigest('MD5', cryptoKey );
        
        updateCreds(EncodingUtil.base64Encode(cryptoKey));
            
        Blob data = Blob.valueOf(PLAIN_TEXT);
        Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey , data);
        String encRequest = EncodingUtil.convertToHex(encryptedData ); 
        
        /*Pass this encRequest with access_code to the https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction using visualforce FORM*/
        
        //**Here is Decryption:**
            
           // Blob cryptoKey2 = Blob.valueOf('WORKING_KEY');
        //Blob hash2 = Crypto.generateDigest('MD5', cryptoKey2);
        Blob data2 = EncodingUtil.convertFromHex(encRequest); //Received from ccAvenue response
        
        Creds_for_Integration__c mcs = [SELECT id, UserName__c FROM Creds_for_Integration__c][0];
        system.debug('mcs'+mcs.UserName__c);
        
        Blob decryptedText = Crypto.decryptWithManagedIV('AES256', EncodingUtil.base64Decode(mcs.UserName__c), data2);
        String PLAIN_TEXT2 = decryptedText.toString();
        
        system.debug('decryptedText' + PLAIN_TEXT2);
    }
    public void namedCred(){
         HttpRequest request = new HttpRequest();
        // This line must be 1st. This is where apex figures out which callout to use.
        request.setEndpoint('callout:Integrate');
        string password =  '{!$Credential.Password}';
        system.debug('password'+password);
    }
}