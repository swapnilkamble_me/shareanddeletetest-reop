public class FirstComponentController {
	@AuraEnabled
    public static list<Account> fetchAccountsNew(){
     	list<Account> accountsList1 = [SELECT Id, Name, Active__c, OwnerId,Industry, Type, Owner.Name FROM Account LIMIT 5];
        system.debug('accountsList1 ::' +accountsList1);
        return accountsList1;
    }
    
    @auraenabled
    public static list<Account> fetchAccountsWithContacts(){
     	list<Account> accountsList = [SELECT Id, Name, (SELECT Id, Name FROM Contacts)FROM Account LIMIT 10];
        return accountsList;
    }
}