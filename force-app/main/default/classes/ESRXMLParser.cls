public class ESRXMLParser {

    private String xmlString;
    public XmlFault fault {get;set;}
    
   
    public class XmlFault {
        public String faultcode {get;set;}
        public String faultstring {get; set;}
    }
    public ESRXMLParser(string xmlString)
    {
        this.xmlString = xmlString;
        fault = new XmlFault();
        
    }
    public void ParseXMLString(){
        string strXml = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">';
            strXml +='<s:Header/><s:Body><s:Fault><faultcode xmlns:a="http://schemas.microsoft.com/ws/2005/05/addressing/none">a:ActionNotSupported</faultcode>';
            strXml +='<faultstring xml:lang="en-US">The message with Action cannot be processed at the receiver</faultstring>';
            strXml +='</s:Fault></s:Body></s:Envelope>';

        try{
            Dom.Document doc = new Dom.Document();
            doc.load(strXml);    
            //return doc;
            Dom.XmlNode node = doc.getRootElement();
            TraceXml(node);
            /*system.debug('doc.getRootElement()'+doc.getRootElement().getChildElements()[1].getName());
            system.debug('doc.getRootElement()'+doc.getRootElement().getChildElements()[1].getChildElements()[0].getChildElements()[0].getName());
            system.debug('doc.getRootElement()'+doc.getRootElement().getChildElements()[1].getChildElements()[0].getChildElements()[0].getText());
            
            if(doc != NULL){
               if(doc.getRootElement() != NULL && doc.getRootElement().size()>2 && doc.getChildElements()[1].getName() == 'Body'){
                    
               }
            }*/
            
           // return;
        }
        
        catch(Exception e){
            //return e.getMessage();
        }
       /*XmlStreamReader objXmlStreamReader = new XmlStreamReader(strXml);
        system.debug(objXmlStreamReader.getNamespace());*/
    }
    
    /*public void TraceXml(Dom.XmlNode node){
        
        for(Dom.XmlNode child : node.getChildElements()) 
        {
            if(child.getName()=='Header')
            {
                // if having multiple header then iterate here otherwise get text()
            }
            else if(child.getName()=='Body')
            {
                TraceXML(child);
            }
            else if(child.getName()=='Fault')
            {
                 for(Dom.XmlNode faultchild : node.getChildElements()) 
                 {
                     if(faultchild.getName()=='faultstring')
                        {
                            fault.faultstring = faultchild.getText();
                        }
                     else
                     {
                         fault.faultcode = faultchild.getText();
                     }
                 }
                TraceXML(child);//Fault                
            }
            else if(child.getName()=='faultcode')
            {
                fault.faultcode = child.getText();
            }
            else if(child.getName()=='faultstring')
            {
                fault.faultcode = child.getText();
            }
            
            return;
        }
    }*/
    
    
    public void TraceXml(Dom.XmlNode envelope){
        for(Dom.XmlNode childEnvelope : envelope.getChildElements()){
            if(childEnvelope.getName()=='Header')
            {
                // if having multiple header then iterate here otherwise get text()
            }
            else if(childEnvelope.getName()=='Body')
            {
                for(Dom.XmlNode childBody : childEnvelope.getChildElements()){
                    if(childBody.getName()=='Fault'){
                         for(Dom.XmlNode faultchild : childBody.getChildElements()){
                             if(faultchild.getName()=='faultcode'){
                                 fault.faultcode = faultchild.getText();
                             }
                             else if(faultchild.getName()=='faultstring'){
                                 fault.faultstring = faultchild.getText();
                             }
                         }         
                    }
                    //actual response need to be parsed here
                }
            }
        }
    }
}