global class scheduledBatchable implements Schedulable {
   global void execute(SchedulableContext sc) {
      //insert new Contact(LastName='test');
      Database.executeBatch(new ExampleBatchClass(), 1);
   }
}