public class importDataFromCSVController {
    
    public String documentName {get;set;}
    public Blob csvFileBody{get;set;}
    public String[] AccountDataLines {get;set;}
    public List <Account> lstAccounts {get;set;}
    public boolean readSuccess {get;set;}
    public List<String> lstFieldNames{get;set;}
    Account conObj;
    String AccountDataAsString;
    String fieldValue;
    Integer fieldNumber;
    Map <String, Integer> fieldNumberMap = new Map < String, Integer > ();
    
    public importDataFromCSVController() 
    {
        documentName = '';
        readSuccess = FALSE;
        AccountDataLines = new String[] {};
            lstAccounts = new List <Account> ();
        lstFieldNames = new List<String>();
    }
    public void readFromFile(){
        try{
            AccountDataAsString = csvFileBody.toString();
            system.debug('AccountDataAsString ' +AccountDataAsString );
            readCSVFile();
        }
        catch(exception e){
            readSuccess = FALSE;
            system.debug('*****Error:'+e );
            system.debug('*****Error e.getCause():'+e.getCause() );
            system.debug('*****Error e.getStackTraceString() :'+e.getStackTraceString() );
            
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR,'Error reading CSV file');
            ApexPages.addMessage(errorMessage);
        }
    }    
    public void readCSVFile() {
        if(lstAccounts != NULL){
            lstAccounts.clear();
        }
        
        AccountDataLines = AccountDataAsString.split('\n');
        string[] csvFieldNames = AccountDataLines[0].split(',');
        for (Integer i = 0; i < csvFieldNames.size(); i++) {
            fieldNumberMap.put(csvFieldNames[i], i);
            lstFieldNames.add(csvFieldNames[i].trim());
        }
        
        system.debug('****AccountDataLines ' +AccountDataLines);
        
        for (Integer i = 1; i < AccountDataLines.size(); i++) {
            conObj = new Account();
            string[] csvRecordData = AccountDataLines[i].split(',');
            for (String fieldName: csvFieldNames) {
                fieldNumber = fieldNumberMap.get(fieldName);
                fieldValue = csvRecordData[fieldNumber];
                conObj.put(fieldName.trim(), fieldValue.trim()); // At this point Field API name is mapped to Field value
            }
            lstAccounts.add(conObj);                
        }
        if(lstAccounts.size() > 0){
            readSuccess = TRUE;
        }
        system.debug('****lstAccounts ' +lstAccounts);
    }
    public void saveData() {
        try {
            //INSERT lstAccounts;
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.INFO, 'Account records inserted successfully');
            ApexPages.addMessage(errorMessage);
        } catch (Exception e) {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'An error has occured while saving data');
            ApexPages.addMessage(errorMessage);
        }
    }
    public PageReference exportToCSV()
    {
        
        PageReference np = new PageReference('/apex/ExportedCSV');
        np.setRedirect(false);
        return np;
    }
}