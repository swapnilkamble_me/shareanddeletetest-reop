public virtual class ParentClass {
    public ParentClass() {
       system.debug('Step 2');
    } 
    public virtual void method1() {
       system.debug('Step 3');
    }
    public void method2() {
        system.debug('Step 1');
        method1();
        system.debug('Step 4');
    }
}