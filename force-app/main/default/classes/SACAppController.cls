public class SACAppController {

    @AuraEnabled
    public static Opportunity init(String oppId) {
        return [SELECT Id, Name, StageName FROM Opportunity WHERE Id = :oppId];
    }
}