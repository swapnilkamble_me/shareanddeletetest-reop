global class ContinuationUsingApexExecute {
    public static AsyncContinuationClass.GetContinuationRespResponse_elementFuture stockQuoteFuture;
    public static Continuation cont;
    
    public static String result {get;set;}

    // Action method
    
   public Static Continuation startRequest() {    
       Integer TIMEOUT_INT_SECS = 60;  
       cont = new Continuation(TIMEOUT_INT_SECS);       
       cont.continuationMethod = 'processResponse';
       system.debug('*****inside startRequest');
       AsyncContinuationClass.AsyncContinuationTestWebservice stockQuoteService = new AsyncContinuationClass.AsyncContinuationTestWebservice();
       stockQuoteFuture = stockQuoteService.beginGetContinuationResp(cont,2); 
       cont.state =    stockQuoteFuture ;
       system.debug('*****inside cont'+cont);
       system.debug('*****inside stockQuoteFuture :'+stockQuoteFuture);
       return cont;   
    }    
    
    // Callback method
    
    public Static String processResponse(Object p) {
    try{   
        system.debug('*****processResponse called stockQuoteFuture::'+stockQuoteFuture);
        stockQuoteFuture = (AsyncContinuationClass.GetContinuationRespResponse_elementFuture)p;
        result = String.valueOf(stockQuoteFuture.getValue());
       
        //result =Continuation.getResponse((String)state);
       
       // Return null to re-render the original Visualforce page
       
       system.debug('*****ContinuationSOAPController ::'+result);
       //insert new Account(Name='AsyncAccount');
       //pagereference redirect = new PageReference('/apex/SubmitPopup');
       return result; 
       }
       catch(Exception e){
           result = 'from catch ========>';
           result += e.getMessage();
           return result;
       }
    }
 }