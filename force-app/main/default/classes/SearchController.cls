public class SearchController{
    @RemoteAction
    public static List<Contact> GetAllContacts( String strKeyword)
    {
        if( String.isBlank(strKeyword))
            return [SELECT Id, Name FROM Contact LIMIT 5];
        else
        {
            strKeyword = '%' + strKeyword + '%';
            return [SELECT Id, Name FROM Contact WHERE Name LIKE :strKeyword LIMIT 5];
        }
    }
}